unit mcblitters;

{                                                                           }
{ Mooncore blitter functions unit                                           }
{ CC0, 2019 : Kirinn Bunnylin / MoonCore                                    }
{ Use freely for anything ever!                                             }
{                                                                           }

interface

type blitstruct = record
       srcp, destp : pointer;
       srcofs, destofs : dword;
       destofsxp, destofsyp : longint;
       copywidth, copyrows : dword;
       srcskipwidth, destskipwidth : dword;
       clipx1p, clipy1p : longint;
       clipx2p, clipy2p : longint;
       clipviewport : dword;
     end;
     pblitstruct = ^blitstruct;

procedure DrawRGB24(clipdata : pblitstruct);
procedure DrawRGBA32(clipdata : pblitstruct);
procedure DrawRGBA32hardlight(clipdata : pblitstruct);
procedure DrawRGBA32alpha(clipdata : pblitstruct; amul : byte);
procedure DrawRGBA32wipe(clipdata : pblitstruct; completion : dword; wipein : boolean);
procedure DrawSolid(clipdata : pblitstruct; fillcolor : dword);
procedure NegateRGB(clipdata : pblitstruct);

var alphamixtab : array[0..255, 0..255] of byte;

implementation

procedure DrawRGB24(clipdata : pblitstruct);
// Copies data from a source bitmap into a destination buffer, while ignoring
// the alpha channel. The source data must be in BGRA byte order.
var destbufbytewidth : dword;
begin
 with clipdata^ do begin
  // Special case for outputbuffy-wide graphics, faster direct blit.
  if (srcskipwidth or destskipwidth) = 0 then
   move(srcp^, destp^, copyrows * copywidth * 4)
  else begin
   // Normal clipped blit.
   copywidth := copywidth * 4;
   inc(srcskipwidth, copywidth);
   destbufbytewidth := copywidth + destskipwidth;
   while copyrows <> 0 do begin
    move(srcp^, destp^, copywidth);
    inc(srcp, srcskipwidth);
    inc(destp, destbufbytewidth);
    dec(copyrows);
   end;
  end;
 end;
end;

procedure DrawRGBA32(clipdata : pblitstruct);
// Copies data from a source bitmap into a destination buffer, applying alpha
// blending normally. The source data must be in BGRA byte order.
var x : dword;
    alpha, inverse : byte;
begin
 with clipdata^ do begin
  while copyrows <> 0 do begin
   x := copywidth;
   while x <> 0 do begin
    alpha := byte((srcp + 3)^);
    case alpha of
      // Shortcut for the majority of pixels, totally transparent or opaque.
      0: begin
          inc(srcp, 4); inc(destp, 4);
         end;
      $FF: begin
            dword(destp^) := dword(srcp^);
            inc(srcp, 4); inc(destp, 4);
           end;
      else begin
       // Partial alpha mix, using the precalculated alphamixtable.
       inverse := alpha xor $FF;
       byte(destp^) := (byte(destp^) * inverse + byte(srcp^) * alpha + 127) div 255;
       inc(srcp); inc(destp);
       byte(destp^) := (byte(destp^) * inverse + byte(srcp^) * alpha + 127) div 255;
       inc(srcp); inc(destp);
       byte(destp^) := (byte(destp^) * inverse + byte(srcp^) * alpha + 127) div 255;
       inc(srcp, 2); inc(destp);
       inc(byte(destp^), alphamixtab[byte(destp^) xor $FF, alpha]);
       inc(destp);
      end;
    end;
    dec(x);
   end;
   inc(srcp, srcskipwidth);
   inc(destp, destskipwidth);

   dec(copyrows);
  end;
 end;
end;

procedure DrawRGBA32hardlight(clipdata : pblitstruct);
// Copies data from a source bitmap into a destination buffer, applying
// "hard light" blending. The source data must be in BGRA byte order.
var x : dword;
    alpha, alphainv, res : byte;
begin
 {$note optimise, stress test hardlight blend}
 with clipdata^ do begin
  while copyrows <> 0 do begin
   x := copywidth;
   while x <> 0 do begin
    alpha := byte((srcp + 3)^);
    alphainv := alpha xor $FF;

    if byte(srcp^) < 128
    then res := byte(srcp^) * byte(destp^) shr 7
    else res := 255 - ((255 - byte(srcp^)) * (255 - byte(destp^))) shr 7;
    byte(destp^) := (byte(destp^) * alphainv + res * alpha) div 255;
    inc(srcp); inc(destp);
    if byte(srcp^) < 128
    then res := byte(srcp^) * byte(destp^) shr 7
    else res := 255 - ((255 - byte(srcp^)) * (255 - byte(destp^))) shr 7;
    byte(destp^) := (byte(destp^) * alphainv + res * alpha) div 255;
    inc(srcp); inc(destp);
    if byte(srcp^) < 128
    then res := byte(srcp^) * byte(destp^) shr 7
    else res := 255 - ((255 - byte(srcp^)) * (255 - byte(destp^))) shr 7;
    byte(destp^) := (byte(destp^) * alphainv + res * alpha) div 255;
    inc(srcp); inc(destp);

    // Increase opaqueness if source is more opaque than destination.
    if byte(srcp^) > byte(destp^) then
    byte(destp^) := (byte(destp^) + byte(srcp^)) shr 1;
    inc(srcp); inc(destp);
    dec(x);
   end;
   inc(srcp, srcskipwidth);
   inc(destp, destskipwidth);

   dec(copyrows);
  end;
 end;
end;

procedure DrawRGBA32alpha(clipdata : pblitstruct; amul : byte);
// Copies 32-bit RGBA data from a source bitmap into a destination buffer,
// multiplying the source bitmap with an alpha value along the way.
// 255 alpha is fully visible, 0 alpha is fully invisible.
var x : dword;
    alpha, inverse : byte;
begin
 with clipdata^ do begin
  while copyrows <> 0 do begin
   x := copywidth;
   while x <> 0 do begin
    alpha := alphamixtab[byte((srcp + 3)^), amul];
    // Shortcut for totally transparent pixels.
    if alpha = 0 then begin
     inc(srcp, 4); inc(destp, 4);
    end else begin
     // Partial alpha mix, using the precalculated alphamixtable.
     // Source must be multiplied by amul.
     // Destination must be multiplied by the inverse of (alpha * amul),
     // then the two are summed.
     inverse := alpha xor $FF;
     byte(destp^) := (byte(destp^) * inverse + byte(srcp^) * alpha + 127) div 255;
     inc(srcp); inc(destp);
     byte(destp^) := (byte(destp^) * inverse + byte(srcp^) * alpha + 127) div 255;
     inc(srcp); inc(destp);
     byte(destp^) := (byte(destp^) * inverse + byte(srcp^) * alpha + 127) div 255;
     inc(srcp, 2); inc(destp);
     inc(byte(destp^), alphamixtab[byte(destp^) xor $FF, alpha]);
     inc(destp);
    end;

    dec(x);
   end;
   inc(srcp, srcskipwidth);
   inc(destp, destskipwidth);

   dec(copyrows);
  end;
 end;
end;

procedure DrawRGBA32wipe(clipdata : pblitstruct; completion : dword; wipein : boolean);
// Copies data from a source bitmap into a destination buffer, applying
// a sideways wipe effect up to 32k completion fraction. If wipein is TRUE,
// the image will appear wiped in from the left; else the image appears being
// wiped away toward the right.
var atable : array of byte;
    x : dword;
    edgewidthp, leadedgep, trailedgep : dword;
    alpha, alpha2 : byte;
begin
 with clipdata^ do begin
  // Pre-calculate an alpha table... This shows what alpha each pixel column
  // needs to be multiplied with.
  setlength(atable, copywidth + 1);
  // If wiping out, invert completion.
  if wipein = FALSE then completion := 32768 - completion;
  // Soft edge width = WinSizeX / 16
  edgewidthp := (copywidth + (destskipwidth shr 2)) shr 4;
  // Lead edge position.
  leadedgep := ((copywidth + edgewidthp) * completion) shr 15;
  // Trailing edge position.
  trailedgep := 0;
  if leadedgep > edgewidthp then trailedgep := leadedgep - edgewidthp;

  if wipein then begin
   // Fill max alpha before trailing edge.
   if trailedgep <> 0 then fillbyte(atable[copywidth - trailedgep], trailedgep, 255);
   // Fill min alpha after leading edge.
   if leadedgep < copywidth then fillbyte(atable[0], copywidth - leadedgep, 0);
   // Prepare the trailing edge alpha value.
   alpha := 255;
   if leadedgep < edgewidthp then alpha := (255 * leadedgep) div edgewidthp;
   // Prepare the leading edge alpha value.
   alpha2 := 0;
   if leadedgep > copywidth then alpha2 := 255 * (leadedgep - copywidth) div edgewidthp;
  end
  else begin
   // Fill min alpha before trailing edge.
   if trailedgep <> 0 then fillbyte(atable[copywidth - trailedgep], trailedgep, 0);
   // Fill max alpha after leading edge.
   if leadedgep < copywidth then fillbyte(atable[0], copywidth - leadedgep, 255);
   // Prepare the trailing edge alpha value.
   alpha := 0;
   if leadedgep < edgewidthp then alpha := ((255 * leadedgep) div edgewidthp) xor $FF;
   // Prepare the leading edge alpha value.
   alpha2 := 255;
   if leadedgep > copywidth then alpha2 := (255 * (leadedgep - copywidth) div edgewidthp) xor $FF;
  end;
  // Clip lead edge.
  if leadedgep > copywidth then leadedgep := copywidth;
  // Calculate the soft edge linear alpha gradient.
  for x := leadedgep - trailedgep downto 0 do
   atable[copywidth - x - trailedgep] := (alpha2 * x + alpha * (leadedgep - trailedgep - x)) div (leadedgep - trailedgep);

  while copyrows <> 0 do begin
   x := copywidth;
   while x <> 0 do begin
    alpha := alphamixtab[byte((srcp + 3)^), atable[x]];
    // Shortcut for totally transparent pixels.
    if alpha = 0 then begin
     inc(srcp, 4); inc(destp, 4);
    end else begin
     // Partial alpha mix, using the precalculated alphamixtable.
     // Source is not premultiplied by its own alpha, must be further
     // multiplied by atable. Destination must be multiplied by the inverse
     // of (alpha * atable), then the two are summed.
     alpha2 := alpha xor $FF;
     byte(destp^) := (byte(destp^) * alpha2 + byte(srcp^) * alpha + 127) div 255;
     inc(srcp); inc(destp);
     byte(destp^) := (byte(destp^) * alpha2 + byte(srcp^) * alpha + 127) div 255;
     inc(srcp); inc(destp);
     byte(destp^) := (byte(destp^) * alpha2 + byte(srcp^) * alpha + 127) div 255;
     inc(srcp, 2); inc(destp);
     inc(byte(destp^), alphamixtab[byte(destp^) xor $FF, alpha]);
     inc(destp);
    end;

    dec(x);
   end;
   inc(srcp, srcskipwidth);
   inc(destp, destskipwidth);

   dec(copyrows);
  end;
 end;
end;

procedure DrawSolid(clipdata : pblitstruct; fillcolor : dword);
// Fills a destination buffer with the alpha profile of a source bitmap,
// using fillcolor.
// Useful for full-screen blackouts, or making a character sprite flash.
// Call ClipRGB first to generate the blitstruct.
var x : dword;
    r, g, b, a, alpha : byte;
begin
 a := byte(fillcolor shr 24);
 if a = 0 then exit;
 b := byte(fillcolor);
 g := byte(fillcolor shr 8);
 r := byte(fillcolor shr 16);

 with clipdata^ do begin
  while copyrows <> 0 do begin
   x := copywidth;
   while x <> 0 do begin
    alpha := alphamixtab[byte((srcp + 3)^), a];
    // Shortcut for totally transparent pixels.
    if alpha = 0 then begin
     inc(srcp, 4); inc(destp, 4);
    end else begin
     // Partial alpha mix, using the precalculated alphamixtable.
     // Source is not premultiplied by its own alpha, must be further
     // multiplied by amul. Destination must be multiplied by the inverse
     // of (alpha * amul), then the two are summed.
     byte(destp^) := byte(alphamixtab[alpha xor $FF, byte(destp^)] + alphamixtab[b, alpha]);
     inc(srcp); inc(destp);
     byte(destp^) := byte(alphamixtab[alpha xor $FF, byte(destp^)] + alphamixtab[g, alpha]);
     inc(srcp); inc(destp);
     byte(destp^) := byte(alphamixtab[alpha xor $FF, byte(destp^)] + alphamixtab[r, alpha]);
     inc(srcp); inc(destp);
     byte(destp^) := byte(alphamixtab[alpha xor $FF, byte(destp^)] + alpha);
     inc(srcp); inc(destp);
    end;

    dec(x);
   end;
   inc(srcp, srcskipwidth);
   inc(destp, destskipwidth);

   dec(copyrows);
  end;
 end;
end;

procedure NegateRGB(clipdata : pblitstruct);
// Negates the destination rectangle given in the input structure.
// Call ClipRGB first to generate the blitstruct.
// (This is only used as a textbox effect)
begin
end;

procedure init;
var i, j : byte;
begin
 // Alphamixtab is a lookup table for anything where you need a*b/255.
 for i := 255 downto 0 do for j := 255 downto 0 do
  alphamixtab[j, i] := i * j div 255;
end;

initialization
 init;
finalization
end.
